package main

import (
	"io/ioutil"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

type page struct {
	Title string
	Body  []byte
}

func main() {
	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello World! Good To See Ya!"))
	})
	r.Get("/save/*", saveHandler)
	r.Get("/view/*", viewHandler)
	r.Get("/sorry/*", sorryHandler)

	http.ListenAndServe(":3333", r)
}

func saveHandler(w http.ResponseWriter, r *http.Request) {
	title := r.URL.Path[len("/save/"):]
	body := r.FormValue("body")
	p := &page{Title: title, Body: []byte(body)}
	err := p.save()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/view/"+title, http.StatusFound)
}

func viewHandler(w http.ResponseWriter, r *http.Request) {
	title := r.URL.Path[len("/view/"):]
	_, err := loadPage(title)
	if err != nil {
		http.Redirect(w, r, "/sorry/"+title+"_does_not_exist", http.StatusFound)
		return
	}
	w.Write([]byte("Your page " + title + " was successfully saved!"))
}

func sorryHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Sorry, the file you are looking for does not exist. Please type /save/<yourfilename> to create a new file."))
}

func (p *page) save() error {
	filename := p.Title + ".txt"
	return ioutil.WriteFile(filename, p.Body, 0600)
}

func loadPage(title string) (*page, error) {
	filename := title + ".txt"
	body, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return &page{Title: title, Body: body}, nil
}
